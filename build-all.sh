#!/bin/sh

mirrordir="/srv/http/mirror"
repodir="${mirrordir}/unsigned"
pkgdir="${repodir}/Packages"


[ ! -d "${pkgdir}" ] && sudo mkdir -p "${pkgdir}"

sudo chown -R "$(id -u)" "${repodir}"

cat > "${mirrordir}"/unsigned.repo <<EOF_YUMREPO
[unsigned]
name=unsigned from podman
baseurl=http://localhost:8080/unsigned/
enabled=1
gpgcheck=0
EOF_YUMREPO

 podman inspect yumrepo >/dev/null 2>&1 \
	|| podman run --name yumrepo  -p 8080:80 -v "${mirrordir}":/usr/share/nginx/html:ro -d nginx



( cd rpm-build && podman build -f Containerfile.rocky8 -t fccagou/rhel8-rpmbuild .  )
( cd mydashboard  && podman build -f Containerfile.rocky8 -t fccagou/rpm/rhel8/build-mydashboard .  )

podman run -ti --rm -v "${pkgdir}":/packages localhost/fccagou/rpm/rhel8/build-mydashboard

podman run -ti --rm -v "${repodir}":/repo --workdir=/repo localhost/fccagou/rhel8-rpmbuild createrepo -d .



