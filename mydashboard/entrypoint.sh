#!/bin/sh

git clone https://gitlab.com/fccagou/mydashboard /tmp/mydashboard \
&& cd /tmp/mydashboard \
&& /bin/sh ./buildrpm \
&& dnf localinstall -y dist/mydashboard-*.noarch.rpm \
&& cp dist/mydashboard-*.noarch.rpm /packages \
